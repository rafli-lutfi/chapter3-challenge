CREATE DATABASE automotive_manufacture;

CREATE TABLE "products" (
  "id" SERIAL,
  "name" VARCHAR(50),
  "quantity" INT,
  PRIMARY KEY ("id")
);

CREATE TABLE "components" (
  "id" SERIAL,
  "name" VARCHAR(50),
  "description" VARCHAR(50),
  PRIMARY KEY ("id")
);

CREATE TABLE "suppliers" (
  "id" SERIAL,
  "component_id" INT,
  "name" VARCHAR(50),
  PRIMARY KEY ("id"),
  CONSTRAINT "FK_suppliers.component_id"
    FOREIGN KEY ("component_id")
      REFERENCES "components"("id")
);

CREATE TABLE "build_products" (
  "component_id" INT,
  "product_id" INT,
  "total_component" INT,
  CONSTRAINT "FK_build_products.component_id"
    FOREIGN KEY ("component_id")
      REFERENCES "components"("id"),
  CONSTRAINT "FK_build_products.product_id"
    FOREIGN KEY ("product_id")
      REFERENCES "products"("id")
);
