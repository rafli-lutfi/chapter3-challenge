INSERT INTO products(name, quantity) 
VALUES
('Honda CBR', 200),
('Honda HRV', 25),
('ATV Honda Sport TRX90X', 50);

INSERT INTO components(name, description)
VALUES
('ban', 'ban terkuat'),
('stang', 'stang motor terbaik'),
('setir', 'setir mobil berkualitas'),
('kaca', 'lamineted 0,76 milimeter'),
('seat', 'seat terbaik');

INSERT INTO suppliers(component_id, name)
VALUES
(1, 'PT. DUNLOP INDONESIA'),
(2, 'PT. Cahaya Indah'),
(4, 'PT. Yasindo Jaya Bersama'),
(1, 'PT. Michelin Indonesia');

INSERT INTO build_products(component_id, product_id, total_component)
VALUES
(1,1,2),
(2,1,1),
(5,1,1),
(1,2,4),
(3,2,1),
(4,2,6),
(5,2,6),
(1,3,4),
(2,3,1),
(5,3,1);

SELECT * FROM suppliers ORDER BY id;

SELECT * FROM components ORDER BY id;

SELECT * FROM products ORDER BY id;

-- menampilkan nama komponen beserta jumlahnya pada tiap produk
SELECT products.name AS nama_produk, components.name AS nama_komponen, build_products.total_component FROM build_products JOIN products ON build_products.product_id = products.id JOIN components ON build_products.component_id = components.id GROUP BY products.id, products.name, components.name, build_products.total_component ORDER BY products.id;

-- menghitung jumlah supplier pada tiap komponen
SELECT components.name AS nama_komponen, count(suppliers.id) AS jumlah_supplier FROM components LEFT JOIN suppliers ON suppliers.component_id = components.id GROUP BY components.id, components.name ORDER BY components.id;

-- mengubah jumlah produk pada tabel produk
UPDATE products SET quantity = quantity + 5 WHERE products.id = 1;

-- mengubah deskripsi pada tabel components
UPDATE components SET description = 'tempered glass' WHERE name = 'kaca';

-- menghapus data supplier pada table supplier
DELETE FROM suppliers WHERE suppliers.name = 'PT. Cahaya Indah';