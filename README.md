# CHALLENGE CHAPTER 3

## SOAL
Pabrikan sebuah perusahaan manufaktur menghasilkan produk. Informasi produk yang disimpan adalah sebagai berikut :

  1. ID produk

  2. Nama produk, dan

  3. Kuantitas

Produk-produk ini terdiri dari banyak komponen. Setiap komponen dapat dipasok oleh satu atau lebih pemasok.
Informasi komponen yang disimpan adalah sebagai berikut :

1. ID komponen

2. Nama komponen

3. Deskripsi

4. Pemasok yang memasoknya

5. Produk yang menggunakannya

Buatlah ERD untuk menunjukkan bagaimana kamu akan
menyimpan informasi ini.

  1. Pemasok dapat eksis tanpa menyediakan komponen.

  2. Komponen tidak harus dikaitkan dengan pemasok.

  3. Komponen tidak harus dikaitkan dengan produk.

  4. Tidak semua komponen digunakan dalam produk.

  5. Suatu produk tidak dapat eksis tanpa komponen.

## ERD Manufacture
 ![Tux, the Linux mascot](/ERD.png)